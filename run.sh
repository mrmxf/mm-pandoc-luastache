# lua project control script (bash)
#
# ./run.sh - prints usage
#
# most of the changes can be done with the variables

# set up some colors and some aliases to make the output pretty
Coff="\e[0m" ; Cblack="\e[30m" ; Cwhite="\e[97m" ; Cyellow="\e[33m" ; Clightgray="\e[37m" ; Cred="\e[31m" ; Cgreen="\e[32m" ; Cblue="\e[34m" ; Cmagenta="\e[35m" ; Ccyan="\e[36m" Cdarkgray="\e[90m" ; Clightred="\e[91m" ; Clightgreen="\e[92m" ; Clightyellow="\e[93m" ; Clightblue="\e[94m" ; Clightmagenta="\e[95m" ; Clightcyan="\e[96m" CX=$Coff ; Ccmd=$Cgreen ; CC=$Ccmd ; Curl=$Ccyan ; CU=$Curl ; Ctxt=$Cyellow ; CT=$Ctxt ; Cinfo=$Cdarkgray ; CI=$Cinfo ; Cerror=$Cred ; CE=$Cerror ; Cwarning=$Clightmagenta ; CW=$Cwarning ; Cfile=$Cwhite ; CF=$Cfile

# set up the lua paths so the modules can be found
# this is preset from `luarocks path`
export LUA_PATH='?;?.lua;lua_modules/share/lua/5.1/?.lua;/home/bluebuntu/.luarocks/share/lua/5.1/?.lua;/home/bluebuntu/.luarocks/share/lua/5.1/?/init.lua;/usr/local/share/lua/5.1/?.lua;/usr/local/share/lua/5.1/?/init.lua;./?.lua;/usr/local/lib/lua/5.1/?.lua;/usr/local/lib/lua/5.1/?/init.lua;/usr/share/lua/5.1/?.lua;/usr/share/lua/5.1/?/init.lua'
export LUA_CPATH='?;?.so;lua_modules/lib/lua/5.1/?.so;/home/bluebuntu/.luarocks/lib/lua/5.1/?.so;/usr/local/lib/lua/5.1/?.so;./?.so;/usr/lib/x86_64-linux-gnu/lua/5.1/?.so;/usr/lib/lua/5.1/?.so;/usr/local/lib/lua/5.1/loadall.so'
export LUA_PATH='?.lua;?;src/?.lua;src/?;lua_modules/share/lua/5.1/?.lua;lua_modules/share/lua/5.1/?'
export LUA_CPATH='?.so;?;lua_modules/lib/lua/5.1/?.so;src/?;src/?.so'

# constants
RAW_FILE="test/test-src/doc01-3.md"
RAW="pandoc --lua-filter=./dist/lustache-doc.lua -t markdown $RAW_FILE"
JSON="pandoc -t json $RAW_FILE"

FOLDER_SRC=src
FOLDER_MODULE=lua_modules
FOLDER_TEST=test

MAIN=main.lua
DIST=dist/lustache-doc.lua

TEST_HELPER=""
LUAROCKS="luarocks install --tree $FOLDER_MODULE"

# Array of dependecies
declare -a DEP=(
  "lustache"
  "json-lua"
  # dev dependencies below here - C & C++ allowed
  "penlight"
  "amalg"
  "inspect"
  "md5"
  "busted")

#Array of modules to build into the distribution file
declare -a MMODULE=(
    "lustache.scanner"
    "lustache.context"
    "lustache.renderer"
    "lustache"
    "JSON"
    "inspect"
    )
declare -a MODULE=(
    "lustache.scanner"
    "lustache.context"
    "lustache.renderer"
    "lustache"
    "JSON"
    "inspect"
    )

# The usage message
fn_usage(){
    echo -e "$Ctxt Usage:$Coff"
    echo -e "     $Cfile\$$Ccmd ./run.sh main            $Ctxt Run the main program$Cfile ./$FOLDER_SRC/$MAIN"
    echo -e "     $Cfile\$$Ccmd ./run.sh test            $Ctxt Busetd test runner in $Cfile ./$FOLDER_TEST"
    echo -e "     $Cfile\$$Ccmd ./run.sh build           $Ctxt Build the final code to$Cfile ./$DIST"
    arraylength=${#MODULE[@]}
    printf "                                 $Ctxt| Modules: $Cfile${MODULE[0]}"
    for (( i=1; i<${arraylength}; i++ )); do
        printf "$Ctxt, $Cfile${MODULE[$i]}$Coff"
    done
    printf "\n"
    echo -e "     $Cfile\$$Ccmd ./run.sh install         $Ctxt Install dependencies to $Cfile ./$FOLDER_MODULE"
    for i in "${DEP[@]}" ; do
      echo -e "                                 $Ctxt| $Ccmd$LUAROCKS $Cfile$i$Coff"
    done
    echo -e "     $Cfile\$$Ccmd ./run.sh raw             $Ctxt Run pandocs raw command$Ccmd .$RAW"
    echo -e "     $Cfile\$$Ccmd ./run.sh ast             $Ctxt Dump the AST as JSON"
}

# if no arguments passed then output usage
if [[ $# -eq 0 ]] ; then fn_usage ; exit 0 ; fi

# Process a command
case $1 in
    json)
        echo -e "${Ctxt}running:$Ccmd $JSONCoff"
        $JSON
    ;;
    raw)
        echo -e "${Ctxt}running:$Ccmd $RAW$Coff"
        $RAW
    ;;
    main)
        lua -l paths $FOLDER_SRC/$MAIN
    ;;
    test)
        CMD="$FOLDER_MODULE/bin/busted --verbose $TEST_HELPER"
        echo -e "${Ctxt}running:$Ccmd $CMD $FOLDER_TEST"
        $CMD $FOLDER_TEST
        ;;
    build)
        CMD=lua_modules/bin/amalg.lua
        CMD="$CMD -o $DIST"
        CMD="$CMD -s $FOLDER_SRC/$MAIN"
        for i in "${MODULE[@]}" ; do
            CMD="$CMD $i"
        done
        echo -e "${Ctxt}running$Ccmd $CMD$Coff"
        $CMD
    ;;
    install)
        for i in "${DEP[@]}" ; do
            CMD="$LUAROCKS $i"
            echo -e "${Ctxt}running$Ccmd $CMD$Coff"
            $CMD
        done
    ;;
    *)    # unknown option
    echo $Cerror Unknown command: $Ccmd$0$Cfile $1$Coff
    fn_usage
    exit 0
    ;;
esac
