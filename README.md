# mm-pandoc-luastache 

Based on https://github.com/Olivine-Labs/lustache from [Olivine Labs](http://olivinelabs.com/lustache/)

This is a simple variable substitution implementation of mustache. More sophisticted templates are not
supported because of the way Pandoc delivers  whole words to filters within the AST. If you want something more
sophisticated use my pandoc-mpp (pmpp) Pandoc Mustache Pre-Processor which supports full mustache
templates for markdown inputs to pandoc. Both sytems can be used together if you want to insert an
array of variables with the pre-processor which are then populated with the lua filter.

## Limitations

* Templates must not contain whitespace:
  * **GOOD** {{parent.child.pet}}
  * **_BAD_** {{ parent.child.pet }}
  