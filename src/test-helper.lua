-- The core configurations
-- ######### ######### ######### ######### ######### ######### ######### #########
helper = {
    dist = {
        filter = "dist/lustache-doc.lua",
    },
    ref = {
        path = "test/test-ref/",
        filter = "test/test-ref/filter",
        fn = "ref"
    },
    src = {
        path = "test/test-src/",
        fn = "doc"
    },
    out = {
        path = "test/test-out/",
        fn = "t01"
    },
    md5 = {
        t01_00_0 = "95442203eb045e49cfb3fc2bf3630b62",
        t02_01_1 = "cc15911efe8912fad230789a20d50863",
        t02_01_2 = "377e1d73be90747441ab9f6492fbcf8e",
        t02_01_3 = "851817faf8fee0db55b9e8aef32dcc9f",
        t03_01_1 = "8b3df4bfc81dc97242137f9133b86810",
        t03_01_2 = "31e0c1a4ef9654226c2e7c2ab4e68608",
        t03_01_3 = "ee1c9b288940ad3bea596af427e4061e",
        t04_01_1 = "8b3df4bfc81dc97242137f9133b86810",
        t04_01_2 = "31e0c1a4ef9654226c2e7c2ab4e68608",
        t04_01_3 = "ee1c9b288940ad3bea596af427e4061e"
    },
    read_file = function(path)
        local file = io.open(path, "r") -- r read mode and b binary mode
        if not file then
            return nil
        end
        local content = file:read "*a" -- *a or *all reads the whole file
        file:close()
        return content
    end,
    write_file = function(path, content)
        local file = io.open(path, "w") -- w write mode
        if not file then
            return nil
        end
        local content = file:write(content) -- write the whole file
        file:close()
        return content
    end
}

-- ######### ######### ######### ######### ######### ######### ######### #########

-- load in a static model from JSON
local JSON = require("JSON")
local json_data = helper.read_file("test/test-src/json01.json")
model = JSON:decode(json_data) -- decode example

-- ######### ######### ######### ######### ######### ######### ######### #########

