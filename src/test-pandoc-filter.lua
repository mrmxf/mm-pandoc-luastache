require("test-helper")
require("test-asserts")
local assert = require("luassert")
local cmd = "pandoc"

-- Create file name templates for input and output files
local s_tpl = helper.src.path .. helper.src.fn
local o_tpl = helper.out.path
-- use pandoc to render a source .md to output .md
--   - check the files exist and are made
--   - check the output file matches the human-qc version
function pandoc_render(txx, id, filter)
    local src_fn = s_tpl .. id .. ".md"
    local out_fn = o_tpl .. txx .. id .. ".md"
    local filter_fn = helper.dist.filter
    local opts = " --lua-filter=" .. filter_fn .. " -f markdown -t markdown "
    assert.file_exists(src_fn)
    assert.file_exists(filter_fn)

    local exe = cmd .. " --output=" .. out_fn .. opts .. src_fn
    print(exe)
    local result = os.execute(exe)
    assert.truthy(result)
    assert.file_exists(out_fn)

    -- make an object key of the form t03_01_1
    local key = txx .. id
    -- replace hyphens with underscores
    key = string.gsub(key, "-", "_")
    assert.file_has_md5(out_fn, helper.md5[key])
end
