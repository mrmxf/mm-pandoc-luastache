-- add the following asserts to busted:
--     file_exists(fn)
--     file_has_md5(fn, hex_str)
--
-- ######### ######### ######### ######### ######### ######### ######### #########
-- create some asserts for the test files:
local say = require("say")
local assert = require("luassert")

local function file_exists(state, arguments)
    if not type(arguments[1]) == "string" then
        return false
    end

    local f = io.open(arguments[1], "r")
    if f ~= nil then
        io.close(f)
        return true
    else
        return false
    end
end

say:set("assertion.file_exists.positive", "File does not exist: %s")
say:set("assertion.file_exists.negative", "File should not exist %s")
assert:register(
    "assertion",
    "file_exists",
    file_exists,
    "assertion.file_exists.positive",
    "assertion.file_exists.negative"
)
-- ######### ######### ######### ######### ######### ######### ######### #########
-- md5 module to check output files are consistent
local md5 = require "md5"
-- io.write (md5._VERSION..' '.._VERSION..'\n')

local function file_has_md5(state, arguments)
    local fn= arguments[1]
    local hash=arguments[2]

    if not type(fn) == "string" then
        return false
    end

    if not type(hash) == "string" then
        return false
    end
    
    local file = io.open(fn, "r") -- r read mode and b binary mode
    if not file then
        return false
    end
    local content = file:read "*a" -- *a or *all reads the whole file
    file:close()

    local_md5_hex= md5.sumhexa(content)

    if local_md5_hex == hash then
        return true
    else
        print("\nmd5(" .. fn ..")==["..local_md5_hex.."] expected ["..hash.."]")
        return false
    end
end

say:set("assertion.file_has_md5.positive", "File %s should have md5: %s")
say:set("assertion.file_has_md5.negative", "File %s should not have md5 %s")
assert:register(
    "assertion",
    "file_has_md5",
    file_has_md5,
    "assertion.file_has_md5.positive",
    "assertion.file_has_md5.negative"
)
-- ######### ######### ######### ######### ######### ######### ######### #########
