local lustache = require "lustache"
-- temporarily pull in a static testmodel
require("test-helper")

local view_model = {
  title = "Joe",
  calc = function ()
    return 2 + 4
  end
}

local output = lustache:render("{{title}} spends {{calc}}", view_model)
print (output)

JSON = require "JSON"
local inspect = require("inspect")

local raw_json_text = '{ "title":"Bruce", "calc":[1,2,3,4]}'

local lua_model = JSON:decode(raw_json_text) -- decode example
local raw_json_text    = JSON:encode(lua_model)        -- encode example
local pretty_json_text = JSON:encode_pretty(lua_model) -- "pretty printed" version

print("-----------------------------")
print(inspect(lua_model))
print(inspect(raw_json_text))
print(inspect(pretty_json_text))

print("-----------------------------")
print (lustache:render("{{title}} spends {{# calc}}= {{.}} {{/calc}}", lua_model))
