-- pandoc lua filter to perform mustche substitutions
--
-- metadata compatible with python pandoc-mustache
--  - https://github.com/michaelstepner/pandoc-mustache
--

local lustache = require "lustache" -- mustache engine
local JSON = require "JSON" -- load json metadata

local LOGGING = PANDOC_STATE and (PANDOC_STATE.verbosity == "WARNING")
local static_metadata_file = "pandoc-metadata.json"

-- our container for the pandoc metadata context
local pd = {
  metadata = {},
  json = {},
  model = {}
}
-- --------------------------------------------------------------------------
local function table_clone_internal(t, copies)
  if type(t) ~= "table" then
    return t
  end

  copies = copies or {}
  if copies[t] then
    return copies[t]
  end

  local copy = {}
  copies[t] = copy

  for k, v in pairs(t) do
    copy[table_clone_internal(k, copies)] = table_clone_internal(v, copies)
  end

  setmetatable(copy, table_clone_internal(getmetatable(t), copies))

  return copy
end

local function table_clone(t)
  -- We need to implement this with a helper function to make sure that
  -- user won't call this function with a second parameter as it can cause
  -- unexpected troubles
  return table_clone_internal(t)
end

local function table_merge(...)
  local tables_to_merge = {...}
  assert(#tables_to_merge > 1, "There should be at least two tables to merge them")

  for k, t in ipairs(tables_to_merge) do
    assert(type(t) == "table", string.format("Expected a table as function parameter %d", k))
  end

  local result = table_clone(tables_to_merge[1])

  for i = 2, #tables_to_merge do
    local from = tables_to_merge[i]
    for k, v in pairs(from) do
      if type(v) == "table" then
        result[k] = result[k] or {}
        assert(type(result[k]) == "table", string.format("Expected a table: '%s'", k))
        result[k] = table_merge(result[k], v)
      else
        result[k] = v
      end
    end
  end

  return result
end

-- --------------------------------------------------------------------------
merge_metadata = function(metadata)
  if (nil == metadata) then
    return
  end
  local new_data = JSON:decode(metadata)
  pd.model = table_merge(pd.model, new_data)
end

get_json_metadata = function(path)
  local file = io.open(path, "r") -- r read mode and b binary mode
  if not file then
    if (LOGGING) then
      print("fail to open JSON ", path)
    end
    return nil
  end
  if (LOGGING) then
    print("loading JSON ", path)
  end
  local content = file:read "*a" -- *a or *all reads the whole file
  file:close()
  return content
end
-- --------------------------------------------------------------------------

-- table dumper
function print_r(t, indent, done)
  done = done or {}
  indent = indent or ""
  local nextIndent  -- Storage for next indentation value
  for key, value in pairs(t) do
    if type(value) == "table" and not done[value] then
      nextIndent = nextIndent or (indent .. string.rep(" ", string.len(tostring(key)) + 2))
      -- Shortcut conditional allocation
      done[value] = true
      print(indent .. "[" .. tostring(key) .. "] => Table {")
      print(nextIndent .. "{")
      print_r(value, nextIndent .. string.rep(" ", 2), done)
      print(nextIndent .. "}")
    else
      print(indent .. "[" .. tostring(key) .. "] => " .. tostring(value) .. "")
    end
  end
end

function get_metadata(meta)
  -- load in a static model if it exists
  merge_metadata(get_json_metadata(static_metadata_file))

  -- first check to see if there is a single mustache path:
  if (meta and meta.mustache and meta.mustache[1] and meta.mustache[1].text) then
    local json_file = pandoc.system.get_working_directory() .. "/" .. meta.mustache[1].text
    merge_metadata(get_json_metadata(json_file))
  end
  -- if mustache is an array the interate through the values
  if (meta and meta.mustache and meta.mustache[1]) then
    for k, v in pairs(meta.mustache) do
        local json_file = pandoc.system.get_working_directory() .. "/" .. v
        print(" metadata file: " .. json_file)
        merge_metadata(get_json_metadata(json_file))
    end
  end
end

-- given a pandoc Str element, this function will replace it using lustache
function replace(el)
  if (el.text) then
    el.text = lustache:render(el.text, pd.model)
    return el
  end
end

-- run the filter on Blocks
return {{Meta = get_metadata}, {Str = replace}}
