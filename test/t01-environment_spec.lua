require("test-helper")
require("test-asserts")
-- Create a variable for this test  module (reduce cut/paste errors)
local txx = "t01-"
-- Create file name templates for input and output files
local s_tpl = helper.src.path .. helper.src.fn
local o_tpl = helper.out.path

local id
local cmd = "pandoc"

describe(
  "Busted environment: ",
  function()
    -- ######### ######### ######### ######### ######### ######### ######### #########
    describe(
      "pandoc",
      function()
        -- --------- --------- --------- --------- --------- --------- ---------
        local id = "00-x"
        it(
          "should return version via pandoc --version(" .. id .. ")",
          function()
            assert.truthy(os.execute(cmd .. " --version > /dev/null"))
          end
        )

        -- --------- --------- --------- --------- --------- --------- ---------
        it(
          "should run a lua filter from frontmatter(" .. id .. ")",
          function()
            id = "00-0"
            local src_fn = s_tpl .. id .. ".md"
            local out_fn = o_tpl .. txx .. id .. ".md"
            local filter_fn = helper.ref.filter .. "00.lua"
            local opts = " --lua-filter=" .. filter_fn .. " -f markdown -t markdown "
            assert.file_exists(src_fn)
            assert.file_exists(filter_fn)
            local result = os.execute(cmd .. " --output=" .. out_fn .. opts .. src_fn)
            assert.truthy(result)
            assert.file_exists(out_fn)
            -- make an object key of the form t03_01_1
            local key= txx .. id
            -- replace hyphens with underscores
            key = string.gsub(key, "-", "_")
            assert.file_has_md5(out_fn, helper.md5[key])
            -- print("failed: ", cmd, out, opts, src)
          end
        )
      end
    ) --pandoc describe

    -- ######### ######### ######### ######### ######### ######### ######### #########
    describe(
      "md5",
      function()
        it(
          "should assert known values",
          function()
            local md5 = require "md5"
            -- test some known sums
            assert.are.same(md5.sumhexa(""), "d41d8cd98f00b204e9800998ecf8427e")
            assert.are.same(md5.sumhexa("a"), "0cc175b9c0f1b6a831c399e269772661")
            assert.are.same(md5.sumhexa("abc"), "900150983cd24fb0d6963f7d28e17f72")
            assert.are.same(md5.sumhexa("message digest"), "f96b697d7cb7938d525a2f31aaf161d0")
            assert.are.same(md5.sumhexa("abcdefghijklmnopqrstuvwxyz"), "c3fcd3d76192e4007dfb496cca67e13b")
            assert.are.same(
              md5.sumhexa("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"),
              "d174ab98d277d9f5a5611c2c9f419d9f"
            )
          end
        )

        it(
          "should assert padding borders",
          function()
            local md5 = require "md5"
            assert.are.same(md5.sumhexa(string.rep("a", 53)), "e9e7e260dce84ffa6e0e7eb5fd9d37fc")
            assert.are.same(md5.sumhexa(string.rep("a", 54)), "eced9e0b81ef2bba605cbc5e2e76a1d0")
            assert.are.same(md5.sumhexa(string.rep("a", 55)), "ef1772b6dff9a122358552954ad0df65")
            assert.are.same(md5.sumhexa(string.rep("a", 56)), "3b0c8ac703f828b04c6c197006d17218")
            assert.are.same(md5.sumhexa(string.rep("a", 57)), "652b906d60af96844ebd21b674f35e93")
            assert.are.same(md5.sumhexa(string.rep("a", 63)), "b06521f39153d618550606be297466d5")
            assert.are.same(md5.sumhexa(string.rep("a", 64)), "014842d480b571495a4a0363793f7367")
            assert.are.same(md5.sumhexa(string.rep("a", 65)), "c743a45e0d2e6a95cb859adae0248435")
            assert.are.same(md5.sumhexa(string.rep("a", 255)), "46bc249a5a8fc5d622cf12c42c463ae0")
            assert.are.same(md5.sumhexa(string.rep("a", 256)), "81109eec5aa1a284fb5327b10e9c16b9")

            assert.are.same(
              md5.sumhexa("12345678901234567890123456789012345678901234567890123456789012345678901234567890"),
              "57edf4a22be3c955ac49da2e2107b67a"
            )
          end
        )
      end
    ) --md5 describe
  end
) -- outer describe
