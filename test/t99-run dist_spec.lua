require("test-helper")
require("test-asserts")
-- Create a variable for this test  module (reduce cut/paste errors)
local txx = "t99-"

describe(
    "Pandoc sub(frontmatter)",
    function()
        -- --------- --------- --------- --------- --------- --------- ---------
        it(
            "should replace something",
            function()
                local cmd = "pandoc"
                local src_fn = "test/test-src/doc01-3.md"
                local out_fn = "test/test-out/t99-out.md"
                local filter_fn = "dist/lustache-doc.lua"
                local opts = " --lua-filter=" .. filter_fn .. " -f markdown -t markdown "
                assert.file_exists(src_fn)
                assert.file_exists(filter_fn)

                local result = os.execute(cmd .. " --output=" .. out_fn .. opts .. src_fn)
            end
        )
    end
)
