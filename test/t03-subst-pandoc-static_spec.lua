require("test-helper")
require("test-asserts")
require("test-pandoc-filter")

-- Create a variable for this test  module (reduce cut/paste errors)
local txx = "t03-"
-- Set the filename for the pandoc lua-filter
local filter_fn = helper.dist.filter

describe(
    "Pandoc(json01)",
    function()
        -- ######### ######### ######### ######### ######### ######### ######### #########
        describe(
            "variables",
            function()
                -- --------- --------- --------- --------- --------- --------- ---------
                it(
                    "should work at root level",
                    function()
                        pandoc_render(txx, "00-1", filter_fn)
                    end
                )

                -- --------- --------- --------- --------- --------- --------- ---------
                it(
                    "should work at level 2",
                    function()
                        pandoc_render(txx, "00-2", filter_fn)
                    end
                )

                -- --------- --------- --------- --------- --------- --------- ---------
                it(
                    "should work at level 3",
                    function()
                        pandoc_render(txx, "00-3", filter_fn)
                    end
                )
            end
        ) --inner describe
    end
) -- outer describe
