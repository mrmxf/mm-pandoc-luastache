require("test-helper")
require("test-asserts")
-- Create a variable for this test  module (reduce cut/paste errors)
local txx = "t02-"
-- Create file name templates for input and output files
local s_tpl = helper.src.path .. helper.src.fn
local o_tpl = helper.out.path
-- md5 module to check output files are consistent
local md5 = require "md5"
-- io.write (md5._VERSION..' '.._VERSION..'\n')
-- lustache bare library
local lustache = require "lustache"

-- use lustache to render a source .md to output .md
--   - check the files exist and are made
--   - check the output file matches the human-qc version
function lustache_render(id)
    local src_fn = s_tpl .. id .. ".md"
    local out_fn = o_tpl .. txx .. id .. ".md"
    assert.file_exists(src_fn)
    local md = helper.read_file(src_fn)
    assert.is_string(md)
    local output = lustache:render(md, model)
    assert.is_string(output)
    helper.write_file(out_fn, output)
    assert.file_exists(out_fn)
    -- make an object key of the form t02_01_1
    local key= txx .. id
    -- replace hyphens with underscores
    key = string.gsub(key, "-", "_")
    assert.file_has_md5(out_fn, helper.md5[key])
end

describe(
    "Lustache sub(helper model): ",
    function()
        -- ######### ######### ######### ######### ######### ######### ######### #########
        describe(
            "variables",
            function()
                -- --------- --------- --------- --------- --------- --------- ---------
                it(
                    "should work at root level",
                    function()
                        lustache_render("00-1")
                    end
                )

                -- --------- --------- --------- --------- --------- --------- ---------
                it(
                    "should work at level 2",
                    function()
                        lustache_render("00-2")
                    end
                )

                -- --------- --------- --------- --------- --------- --------- ---------
                it(
                    "should work at level 3",
                    function()
                        lustache_render("00-3")
                    end
                )
            end
        ) --inner describe
    end
) -- outer describe
